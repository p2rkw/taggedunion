# Tagged Union #

### What is tagged union? ###
Tagged union is data type that can hold several different data-types. Learn more on wikipedia: http://en.wikipedia.org/wiki/Tagged_union
Goal of this project is to implement tagged unions in C++11.

### Usage ###
TaggedUnion have ability to match to function with same argument type. This guarantees easy and type safe access to value stored by union.
```
#!c++
TaggedUnion<int, float, std::string> tu(4.2f);
    int id;
    bool status = tu.match(
      [&](int){ id = 1; },
      [&](float){ id = 2; }, // only this lambda will be executed because it's argument have same type as union object `tu` .
      [&](const std::string&){ id = 3; }
    );
    assert(status == true); // it means that something was matched
    assert(id == 2);

```

### How to use ###
* Clone this repository: git clone https://p2rkw@bitbucket.org/p2rkw/taggedunion.git
* Go to directory with sources and call make
