#pragma once

template <class T, T... Ints>
struct integer_sequence{
  static constexpr std::size_t size() { return sizeof...(Ints); }
};

template<std::size_t... Ints>
using index_sequence = integer_sequence<std::size_t, Ints...>;

namespace impl{

template <class T, bool IsEnd, T End, T... Seq>
struct SequenceMakerImpl : SequenceMakerImpl<T, (End-1) == 0, End-1, End-1, Seq...> {};

template <class T, T End, T... Seq>
struct SequenceMakerImpl<T, true, End, Seq...> {
  using type = integer_sequence<T, Seq...>;
};

template <class T, T End>
struct SequenceMaker : SequenceMakerImpl<T, End == 0, End> {};

} /* namespace impl */

template<class T, T N>
using make_integer_sequence = typename impl::SequenceMaker<T, N>::type;
		
template<std::size_t N>
using make_index_sequence = make_integer_sequence<std::size_t, N>;