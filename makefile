CXXFLAGS := -Wall -Wextra -std=c++11 -fno-strict-aliasing -O2

all: tests

tests: tests.cpp tagged-union.h first-arg-type.h
	$(CXX) $(CXXFLAGS) $< -o $(@)
	
clean:
	rm tests
