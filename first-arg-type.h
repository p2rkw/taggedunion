#pragma once

#include <functional>

namespace impl{
template<class F>
struct FunctionTypesDeductor{
  using FirstArg = typename FunctionTypesDeductor<decltype(&F::operator())>::FirstArg;
  using ReturnType = typename FunctionTypesDeductor<decltype(&F::operator())>::ReturnType;
};

template<class R, class T0, class... T>
struct FunctionTypesDeductor<std::function<R(T0, T...)>>{
  using FirstArg = T0;
  using ReturnType = R;
};

template<class R, class T0>
struct FunctionTypesDeductor<std::function<R(T0)>>{
  using FirstArg = T0;
  using ReturnType = R;
};

template<class R, class T, class A0, class... A>
struct FunctionTypesDeductor<R(T::*)(A0, A...)>{
  using FirstArg = A0;
  using ReturnType = R;
};

template<class R, class T, class A0, class... A>
struct FunctionTypesDeductor<R(T::*)(A0, A...) const>{
  using FirstArg = A0;
  using ReturnType = R;
};

template<class R, class A0, class... A>
struct FunctionTypesDeductor<R(*)(A0, A...)>{
  using FirstArg = A0;
  using ReturnType = R;
};
} /* namespace impl */

template <class F>
using FirstArg = typename impl::FunctionTypesDeductor<F>::FirstArg;

template <class F>
using ReturnType = typename impl::FunctionTypesDeductor<F>::ReturnType;
