#include "tagged-union.h"
#include "integer-sequence.h"

#include <cassert>
#include <type_traits>
#include <tuple>

void firstArgTests(){
  auto asdf = [](const char *){};
  static_assert(std::is_same<FirstArg<decltype(asdf)>, const char*>(), "FirstArg with lambda");

  struct A{ static void S(float); };
  static_assert(std::is_same<FirstArg<decltype(&A::S)>, float>(), "FirstArg with struct");
  static_assert(std::is_same<FirstArg<void(*)(int)>, int>(), "FirstArg with function pointer");
  
  // doest work with references yet, I don't need it
  //static_assert(std::is_same<FirstArg<void(int)>, int>(), "FirstArg with function pointer");
}

#include <cstdio>
void integerSequenceTests(){
  static_assert(std::is_same<make_index_sequence<0>, index_sequence<>>(), "index sequence 0");
  static_assert(std::is_same<make_index_sequence<1>, index_sequence<0>>(), "index sequence 1");
  static_assert(std::is_same<make_index_sequence<2>, index_sequence<0,1>>(), "index sequence 2");
  static_assert(std::is_same<make_index_sequence<3>, index_sequence<0,1,2>>(), "index sequence 3");
  static_assert(make_index_sequence<0>::size() == 0, "0 sequence size");
  static_assert(make_index_sequence<3>::size() == 3, "3 sequence size");
}

int main(){
  firstArgTests();
  integerSequenceTests();
  
  int id;
  auto matchers = std::make_tuple(
    [&](int){ id = 1; },
    [&](float){ id = 2; },
    [&](const std::string&){ id = 3; }
  );
      
  {
    TaggedUnion<int, float, std::string> tu;
    int id;
    bool status = tu.match(
      [&](int){ id = 1; },
      [&](float){ id = 2; },
      [&](const std::string&){ id = 3; }
    );
    assert(status);
    assert(id == 1);
  }
  
  {
    TaggedUnion<int, float, std::string> tu(0.f);
    id = -1;
    bool status = tu.matchT(matchers);
    assert(status);
    assert(id == 2);
  }
  
  {
    TaggedUnion<int, float, std::string> tu(0.f);
    id = -1;
    bool status = tu.matchT(matchers);
    assert(status);
    assert(id == 2);
    
    const char* asd = "asd";
    tu = asd;
    assert(status);
    assert(id == 3);
    
    // todo: 
    //tu = "asd";
    //assert(status);
    //assert(id == 3);
  }
  
  {
    // todo:
    //TaggedUnion<int, float, std::string> tu = 1.f;
    //bool status = tu.matchT(matchers);
    //assert(status);
    //assert(id == 2);
  }
  
  puts("tests ok");
  return 0;
}/*main*/