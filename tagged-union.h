#pragma once 

#include <tuple>
#include <cassert>
#include <type_traits>

#include "first-arg-type.h"
// remove when it will became availible in GCC
#include "integer-sequence.h"

namespace impl{
template <class L, class R>
struct is_similar : std::integral_constant<bool, std::is_same<L, R>() || std::is_constructible<L, R>()  || std::is_convertible<L,R>() >{};

template <class W, bool Found, class T0, class... T>
struct TypeIndexGetter : 
          std::integral_constant<intptr_t,
                                 is_similar<T0, W>() ? 0 : 
                                                         TypeIndexGetter<W, Found || std::is_same<W, T0>(), T...>() + 1>
{};

template <class W, bool Found, class T0>
struct TypeIndexGetter<W, Found, T0> : std::is_same<T0, W>{
  static_assert(Found || is_similar<T0, W>(), "Type T0 not present in tagged union");
};
} /* namespace impl */

/** It will return index of first occurence of given type 'W' inside pack 'T...'
 */
template <class W, class... T>
constexpr int getTypeIndex(){
  return impl::TypeIndexGetter<W, false, T...>();
}

/**
 * Objects of this type will reserve enough space for biggest type given.
 * See: http://en.cppreference.com/w/cpp/types/aligned_union
 * todo: remove when std::aligned_union will became availible in GCC
 */
template<class T0, class... T>
struct aligned_union{
  union U{
    T0 t0;
    aligned_union<T...> t1;
  };
};

template <class T>
struct unpack_tuple{};

template <size_t... S>
struct unpack_tuple<index_sequence<S...>>{
  template<class F, class... T>
  static
  ReturnType<F> exec(const std::tuple<T...>& tuple, F&& action){
    return action(std::get<S>(tuple)...);
  }
};

template <class F, class... T>
constexpr ReturnType<F> unpackTuple(const std::tuple<T...>& tuple, F&& action){
  return unpack_tuple<make_index_sequence<sizeof...(T)>>::exec(tuple, std::forward<F>(action));
}


/**
 * Type safe union representation. It will guarantee destructor calls, and 
 * provide match method.
 */
template <class... T>
struct TaggedUnion{
  
  /**
   * Static template that will generate call of destructor of given type W
   * @param data object to destroy
   */
  template<class W>
  static void destructorCaller(void* data){
    W* w = (W*)(data);
    w->~W();
  }
  typedef void(*DestructorCaller)(void*);
  
  template <class W>
  using FirstArg_ = typename std::remove_const<typename std::remove_reference<FirstArg<W>>::type>::type;
  
  /** Type of first argument */
  using T0 = FirstArg_<void(*)(T...)>; // get first type from pack without splitting it
  
  /**
   * Generates identifier for given type.
   * If type has trivial destructor then it it doesn't have to be called when
   * type of union change. 
   * When type W has non trivial destructor, it's address can be used as 
   * identifier. 
   * Due to fact that address of destructor can't be taken, I used
   * destructorCaller<W> instead.
   * @return address of 'destructor'.
   */
  template <class W>
  static constexpr DestructorCaller getTypeDestructor(){
    static_assert(getTypeIndex<W, T...>() <= sizeof...(T), "Type 'W' not present in this tagged union" );
    return &destructorCaller<W>;
  }
  
  /**
   * Calls stored destructor if type has non trivial one.
   */
  template <class W>
  void destroy(){
    if(std::is_trivially_destructible<W>()){
      destructor(&data);
    }
  }
  
protected:
  //! replace with 'std::aligned_union<4, T...> data;' when it will became availible in GCC
  /** Union build with given types, storage for objects. */
  aligned_union<T...> data;
  
  /** Destructor for current type wrapped into function */
  DestructorCaller destructor = getTypeDestructor<T0>();
  
public:  
  /**
   * Will initialize first type aliased as 'T0'
   */
  TaggedUnion() = default;
  
  template<class W>
  TaggedUnion(W&& w) : destructor(getTypeDestructor<W>()) {
    new (&data) W(std::forward<W>(w));
  }
  
  ~TaggedUnion(){
    // I have to call destructor here, even if it's trivial, 
    // because i can't prove it.
    destructor(&data);
  }
  
  /**
   * Destroys actualy stored object and replaces it with given argument.
   * When actual type and W are the same types it will call assignment opertor.
   * @param w new value of this union
   * @return *this
   */
  template <class W>
  TaggedUnion<T...>& operator= (W&& w){
    using PW = typename std::remove_const<typename std::remove_reference<W>::type>::type;
    if(getTypeDestructor<PW>() == destructor){
      // actually stored object have same type as argument 'w'.
      // Can use assignment operator.
      PW& current = *(PW*)&data;
      current = std::forward<W>(w);
      return *this;
    }
    destroy<PW>();
    new (&data) PW(std::forward<W>(w));
    destructor = getTypeDestructor<PW>();
    return *this;
  }
  
  /**
   * Will execute one of given actions when type of it's first argument match
   * with type of actually stored object.
   * @param action first given action 
   * @param actions rest of actions
   * @return true if something was executed (todo: it may returns index of type 'W')
   */
  
  template <class F0, class... F>
  bool match(F0&& action, F&&... actions){
    return match(std::forward<F0>(action)) ? true : match(std::forward<F>(actions)...); 
  }
  
  /*@{*/
  template <class F0, class... F>
  bool matchT(const std::tuple<F0, F...>& actions){
    return unpackTuple(actions, [this](const F0& a0, const F&... rest){
      return this->match(a0, rest...);
    });
  }
  /*@}*/

  /**
   * If data can be converted to first argument of action then it will be called
   * @param action action to take
   * @return true if action was executed false otherwise
   */
  /*@{*/
  template <class F0>
  bool match(F0&& action){
    using FA = FirstArg_<F0>;
    if(getTypeDestructor<FA>() == destructor){
      action(*(FA*)&data);
      return true;
    }
    return false;
  }
  
  template <class F0>
  bool match(const F0& action){
    using FA = FirstArg_<F0>;
    if(getTypeDestructor<FA>() == destructor){
      action(*(FA*)&data);
      return true;
    }
    return false;
  }
  /*@}*/
  
};
